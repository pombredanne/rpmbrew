from __future__  import print_function
from os.path import expanduser
import os.path
import sys


EXISTS_AS_DIR = 1
EXISTS_AS_FILE = 2
DOES_NOT_EXIST = 0

def create_path(path):
    import os.path as os_path
    paths_to_create = []
    while not os_path.lexists(path):
        paths_to_create.insert(0, path)
        head,tail = os_path.split(path)
        if len(tail.strip())==0: # Just incase path ends with a / or \
            path = head
            head,tail = os_path.split(path)
        path = head

    for path in paths_to_create:
        os.mkdir(path)
        
def check_path(path):

    if not os.path.isdir(path):
        if os.path.exists(path):
            return EXISTS_AS_FILE
        else:
            return DOES_NOT_EXIST
    else:
        return EXISTS_AS_DIR
        
        
        
def setup():
    
    """Setup the home directory and rpmmacros. 
    Similar to running rpmdev-setuptree"""

    def create_subdirs(rpmbuild_dirs, rpmdir):
        for entry in rpmbuild_dirs:
            
            entry_path = os.path.join(rpmdir, entry)
            entry_status = check_path(entry_path)
            
            if entry_status == EXISTS_AS_FILE:
                msg = "Found {} as a file. Removing it."
                print(msg)
                os.remove(entry_path)
                os.mkdir(entry_path)
            elif entry_status == DOES_NOT_EXIST:
                print("Creating {}".format(entry_path))
                os.mkdir(entry_path)
                
            elif entry_status == EXISTS_AS_DIR:
                print("{} already exists.. Skipping".format(entry_path))
    
    rpmbuild_dirs = ["BUILD","RPMS","SOURCES","SPECS","SRPMS"]
    
    home_dir = expanduser("~")
    rpmdir = os.path.join(home_dir,"rpmbuild")
    
    rpmdir_status = check_path(rpmdir)
    
    if  rpmdir_status == EXISTS_AS_FILE :
        msg = """File: ~/rpmdir exists. Delete this and try again."""
        print(msg)
        sys.exit(1)    
            
    elif rpmdir_status == EXISTS_AS_DIR: 
        
        msg = "~/rpmbuild exists .. Skipping"
        print(msg)
        
        create_subdirs(rpmbuild_dirs, rpmdir)
        
    elif rpmdir_status == DOES_NOT_EXIST : 

        print("Creating rpmbuild directories.")
        os.mkdir(rpmdir)
        create_subdirs(rpmbuild_dirs, rpmdir)

 
    rpmmacros_path = os.path.join(home_dir, ".rpmmacros")
    rpmmacros_path_status =  check_path(rpmmacros_path)
    
    if rpmmacros_path_status == EXISTS_AS_FILE:
        print("RPM Macros already exist. Not deploy our copy.")
        
    else: 
        
        filename = "rpmbrew.rpmmacros"
        print("Deploying files/rpmbrew.rpmmacros")
        import shutil
        
        BASE_DIR = os.path.dirname(__file__)
        src_file = os.path.join(BASE_DIR, "files", filename)
        
        shutil.copy2(src_file, os.path.join(home_dir,".rpmmacros"))
        
        
if __name__ == "__main__":
    

    setup()